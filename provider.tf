provider "azurerm" {
  features {}
}


# State Backend
terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "nathantfstate"
    container_name       = "tfstate"
    key                  = "nathan-terraform.tfstate"
  }
}
